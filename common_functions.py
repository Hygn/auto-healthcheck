def DBCreate():
    import sqlite3
    #connect/create sqlite3 Database
    print('Connecting to Database')
    c = sqlite3.connect('user.db')
    cu = c.cursor()
    try:
        print('Creating Table')
        cu.execute('CREATE TABLE users (name TEXT, birthday TEXT, orgCode TEXT, password TEXT, orgUrl TEXT)')
    except:
        print('Error!! Table Already Exists!')
    c.commit()
    c.close
    return
def writeToDB(name,bday,orgc,pw,orgurl):
    import sqlite3
    #connect/create sqlite3 Database
    print('Connecting to Database')
    c = sqlite3.connect('user.db')
    cu = c.cursor()
    try:
        print('Writing Into Database')
        cu.execute('INSERT INTO users VALUES (?,?,?,?,?)',(name,bday,orgc,pw,orgurl))
    except Exception as e:
        print('Error Writing Database\n'+e)
    c.commit()
    c.close
    return
def readFromDB():
    import sqlite3
    #connect/create sqlite3 Database
    print('Connecting to Database')
    c = sqlite3.connect('user.db')
    cu = c.cursor()
    try:
        print('Reading From Database')
        cu.execute('SELECT * FROM users')
        all_ = cu.fetchall()
    except:
        print('Error Reading Database')
    c.commit()
    c.close
    return all_
def findFromDB(name,bday,orgc,pw,orgurl):
    import sqlite3
    #connect/create sqlite3 Database
    print('Connecting to Database')
    c = sqlite3.connect('user.db')
    cu = c.cursor()
    try:
        print('Searching From Database')
        cu.execute('SELECT * FROM users WHERE name=? AND birthday=? AND orgCode=? AND password=? AND orgUrl=?',(name,bday,orgc,pw,orgurl))
        data = cu.fetchall()
    except:
        print('Error Reading Database')
    c.commit()
    c.close
    return data
def deleteFromDB(name,bday,orgc,pw,orgurl):
    import sqlite3
    #connect/create sqlite3 Database
    print('Connecting to Database')
    c = sqlite3.connect('user.db')
    cu = c.cursor()
    try:
        print('Deleting From Database')
        cu.execute('DELETE FROM users WHERE name=? AND birthday=? AND orgCode=? AND password=? AND orgUrl=?',(name,bday,orgc,pw,orgurl))
    except:
        print('Error Delete from Database')
    c.commit()
    c.close
    return
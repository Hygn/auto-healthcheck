import encrypt
import requests
import json
import os
#https://www.youtube.com/watch?v=f4ZRK8YLmPc
#https://www.youtube.com/watch?v=aGuHixFujHE
#https://youtu.be/TiCEzvE0trc?t=922
#https://www.youtube.com/watch?v=ljmGSQvYeGo
#https://www.youtube.com/watch?v=TyHITBLVrEg
#https://www.youtube.com/watch?v=1QMfcLcH56o
#https://www.youtube.com/watch?v=6iseNlvH2_s
header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36',
          'X-Requested-With': 'XMLHttpRequest'}
defaultSurveyPayload = {"rspns01":"1",
                        "rspns02":"1",
                        "rspns03":None,
                        "rspns04":None,
                        "rspns05":None,
                        "rspns06":None,
                        "rspns07":None,
                        "rspns08":None,
                        "rspns09":"0",
                        "rspns10":None,
                        "rspns11":None,
                        "rspns12":None,
                        "rspns13":None,
                        "rspns14":None,
                        "rspns15":None,
                        "rspns00":"Y",
                        "deviceUuid":"",}
def schoolSearch(lctnScCode=0,schulCrseScCode=0,orgName=''):
    payload = {'lctnScCode':lctnScCode,'schulCrseScCode':schulCrseScCode,'orgName':orgName,'loginType':'school'}
    jsonData = requests.get('https://hcs.eduro.go.kr/v2/searchSchool',params=payload,headers=header).content.decode('utf-8')
    parsed = json.loads(jsonData)
    for i in parsed['schulList']:
        yield {'schoolCode':i['orgCode'],'schoolNameKR':i['kraOrgNm'], 'schoolNameEN': i['engOrgNm'], 'schoolAddress':i['addres'],'atptOfcdcConctUrl':i['atptOfcdcConctUrl']}
    return
def findUsr(birthday,name,orgCode,atptOfcdcConctUrl,pubkey=None):
    if pubkey == None:
        pubkey = open('.tmp.pem','r').read()
    encBday = encrypt.encrypt(pubkey,birthday).decode('utf-8')
    encName = encrypt.encrypt(pubkey,name).decode('utf-8')
    payload = {'birthday':encBday,
               'loginType':'school',
               'name':encName,
               'orgCode':orgCode,
               'stdntPNo':None}
    data = requests.post(f"https://{atptOfcdcConctUrl}/v2/findUser",json=payload,headers=header).content.decode('utf-8')
    return json.loads(data)
def hasPW(token,atptOfcdcConctUrl):
    _header = header
    _header.update({'Authorization':token})
    return requests.post(f"https://{atptOfcdcConctUrl}/v2/hasPassword",json={},headers=_header).content.decode('utf-8')
def validatePW(token,pw,atptOfcdcConctUrl,pubkey=None):
    _header = header
    _header.update({'Authorization':token})
    if pubkey == None:
        pubkey = open('.tmp.pem','r').read()
    encpw = encrypt.encrypt(pubkey,pw).decode('utf-8')
    payload = {'deviceUuid': '',
               'password': encpw}
    return requests.post(f"https://{atptOfcdcConctUrl}/v2/validatePassword",json=payload,headers=_header).content.decode('utf-8')
def selUsrGroup(token,atptOfcdcConctUrl):
    _header = header
    _header.update({'Authorization':token})
    return json.loads(requests.post(f"https://{atptOfcdcConctUrl}/v2/selectUserGroup",json={},headers=_header).content.decode('utf-8'))
def getUsrInfo(token,orgCode,userPNo,atptOfcdcConctUrl):
    _header = header
    _header.update({'Authorization':token})
    payload = {'orgCode': orgCode,
               'userPNo': userPNo}
    return json.loads(requests.post(f"https://{atptOfcdcConctUrl}/v2/getUserInfo",json=payload,headers=_header).content.decode('utf-8'))
def submitSurvey(token,name,atptOfcdcConctUrl,surveyPayload=None):
    _header = header
    _header.update({'Authorization':token})
    if surveyPayload == None:
        payload = defaultSurveyPayload
    else:
        payload = surveyPayload
    payload.update({'upperToken':token,
                    'upperUserNameEncpt': name})
    return json.loads(requests.post(f"https://{atptOfcdcConctUrl}/registerServey",json=payload,headers=_header).content.decode('utf-8'))
def detectQuestionChange():
    try:
        data = requests.get('https://rl6cz18qh.toastcdn.net/eduro/1.6.0/js/app.7c401385.js',headers=header).content.decode('utf-8')
        question = data.split('null,surveyResult:')[1].split('}},')[0].replace(' ','')
        defaultQues = open(os.getcwd()+'/defaultQues','r',encoding='utf-8').read().replace(' ','')
        ismatch = defaultQues in question
        surveyItem = data.split('ko:{stdnt:{guidContents:["')[1].split('"},en:')[0].replace(' ','').replace('\\n','')
        defaultSurvItem = open(os.getcwd()+'/defaultServItem','r',encoding='utf-8').read().replace(' ','').replace('\n','')
        ismatch2 = defaultSurvItem in surveyItem
    except IndexError:
        ismatch,ismatch2 = False,False
    return ismatch and ismatch2
def updateQuestionChange():
    open('defaultQues','a')
    open('defaultServItem','a')
    data = requests.get('https://rl6cz18qh.toastcdn.net/eduro/1.6.0/js/app.7c401385.js',headers=header).content.decode('utf-8')
    open('defaultQues','w',encoding='utf-8').write(data.split('null,surveyResult:')[1].split('}},')[0].replace(' ',''))
    open('defaultServItem','w',encoding='utf-8').write(data.split('ko:{stdnt:{guidContents:["')[1].split('"},en:')[0].replace(' ','').replace('\\n',''))
    return

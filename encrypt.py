def get_privKey():
    import requests
    header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36'}
    index = requests.get('https://hcs.eduro.go.kr/#/loginWithUserInfo',headers=header).content.decode('utf-8')
    cdn_subdomain = index.split('script src=https://')[1].split('.toastcdn.net/eduro')[0]
    cdn_ver = index.split(f'https://{cdn_subdomain}.toastcdn.net/eduro/')[1].split('/')[0]
    appjs_name = 'app.'+index.split(f'<script src=https://{cdn_subdomain}.toastcdn.net/eduro/{cdn_ver}/js/app.')[1].split('>')[0]
    appjs_cont = requests.get(f'https://{cdn_subdomain}.toastcdn.net/eduro/{cdn_ver}/js/{appjs_name}').content.decode('utf-8')
    privkey = appjs_cont.split('privateKey:"')[1].split('"')[0]
    open('.tmp.der','a')
    open('.tmp.der','w').write(privkey)
    return privkey
def der2pem(pubkey):
    import os
    import sys
    import binascii
    import subprocess
    if sys.platform.lower() != 'linux':
        raise EnvironmentError
    pubkey = binascii.unhexlify(pubkey)
    try:
        open('.tmp.der', 'a')
    except:
        pass
    open('.tmp.der', 'wb').write(pubkey)
    subprocess.call('openssl rsa -pubin -in .tmp.der -inform der -out .tmp.pem -outform pem', shell=True, stdout=open(os.devnull,'wb'))
    return open('.tmp.pem','r').read()
def encrypt(pubkey,plaintext,encoding='utf-8'):
    from Crypto.Cipher import PKCS1_v1_5
    from Crypto.PublicKey import RSA
    import base64
    rsa_key = RSA.importKey(pubkey)
    cipher = PKCS1_v1_5.new(rsa_key)
    plaintext = plaintext.encode(encoding)
    return base64.b64encode(cipher.encrypt(plaintext))

#코드 보기 전에 나나히라 구독하고 보세오
#https://www.youtube.com/channel/UC_fYA9QRK-aJnFTgvR_4zug
import schedule
import time
import API
import random
import common_functions as cf

def job():
    time.sleep(random.randrange(0,20)*60)
    if API.detectQuestionChange() != True:
        print('Error Question Changed!!')
        quit()
    usrlist = cf.readFromDB()
    random.shuffle(usrlist)
    for i in usrlist:
        usrdata = API.findUsr(i[1],i[0],i[2],i[4])
        token = API.validatePW(usrdata['token'],i[3],i[4])
        usrgroup = API.selUsrGroup(token.replace('"',''),i[4])[0]
        API.getUsrInfo(usrgroup['token'],i[2],usrgroup['userPNo'],i[4])
        API.submitSurvey(token.replace('"',''),i[0],i[4])
        chk = API.getUsrInfo(usrgroup['token'],i[2],usrgroup['userPNo'],i[4])
        print(f"{i[0]} {i[1]} {i[2]} {i[3]} {chk['isHealthy']}")
        if chk['isHealthy'] != True:
            break
schedule.every().day.at("07:10").do(job)
while True:
    schedule.run_pending()
    time.sleep(1)
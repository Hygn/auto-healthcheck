import API
import sys
import random
import common_functions as cf
try:
    if sys.argv[1] == '-c':
        cf.DBCreate()
    def findSchool():
        print('''
01 . 서울특별시
02 . 부산광역시
03 . 대구광역시
04 . 인천광역시
05 . 광주광역시
06 . 대전광역시
07 . 울산광역시
08 . 세종특별자치시
10 . 경기도
11 . 강원도
12 . 충청북도
13 . 충청남도
14 . 전라북도
15 . 전라남도
16 . 경상북도
17 . 경상남도
18 . 제주특별자치도''')
        while True:
            lctnSc = input('input loc: ').strip()
            try:
                if 0 < int(lctnSc) < 19 and int(lctnSc) != 9:
                    if int(lctnSc) < 10:
                        lctnSc = '0'+str(int(lctnSc))
                    break
                else:
                    raise Exception
            except:
                print('제대로 값을 입력하세요!')
        print('''
1 . 유치원
2 . 초등학교
3 . 중학교
4 . 고등학교
5 . 특수학교 등''')
        while True:
            crseSc = input('input loc: ').strip()
            try:
                if 0 < int(crseSc) < 6:
                    break
                else:
                    raise Exception
            except:
                print('제대로 값을 입력하세요!')
        searchParam = input('input param: ')
        count = 0
        srcres = API.schoolSearch(lctnSc,crseSc,searchParam)
        list_ = []
        for i in srcres:
            list_.append(i)
            print(str(count) +' . '+ i['schoolNameKR'])
            print(i['schoolAddress'])
            print('\n')
            count += 1
        if len(list_)< 1:
            print('검색결과가 없습니다')
            quit()
        while True:
            try:
                idx = input('input index: ')
                if -1 < int(idx) < count + 1:
                    data = list_[int(idx)]
                    break
                else:
                    raise Exception
            except:
                print('제대로 값을 입력하세요!')
        return data
    if sys.argv[1] == '-a':
        schooldata = findSchool()
        name = input('name: ')
        birthday = input('birthday: ')
        usrdata = API.findUsr(birthday,name,schooldata['schoolCode'],schooldata['atptOfcdcConctUrl'])
        if API.hasPW(usrdata['token'],schooldata['atptOfcdcConctUrl']) != 'true':
            print('사용자 설정후 이용해주세요!')
            quit()
        pw = input('input pw: ')
        token = API.validatePW(usrdata['token'],pw,schooldata['atptOfcdcConctUrl'])
        cf.writeToDB(name,birthday,schooldata['schoolCode'],pw,schooldata['atptOfcdcConctUrl'])
    if sys.argv[1] == '-d':
        schooldata = findSchool()
        name = input('name: ')
        birthday = input('birthday: ')
        pw = input('input pw: ')
        cf.deleteFromDB(name,birthday,schooldata['schoolCode'],pw,schooldata['atptOfcdcConctUrl'])
    if sys.argv[1] == '-ru':
        if API.detectQuestionChange() != True:
            print('Error Question Changed!!')
            quit()
        usrlist = cf.readFromDB()
        random.shuffle(usrlist)
        for i in usrlist:
            usrdata = API.findUsr(i[1],i[0],i[2],i[4])
            token = API.validatePW(usrdata['token'],i[3],i[4])
            usrgroup = API.selUsrGroup(token.replace('"',''),i[4])[0]
            API.getUsrInfo(usrgroup['token'],i[2],usrgroup['userPNo'],i[4])
            API.submitSurvey(token.replace('"',''),i[0],i[4])
            chk = API.getUsrInfo(usrgroup['token'],i[2],usrgroup['userPNo'],i[4])
            print(f"{i[0]} {i[1]} {i[2]} {i[3]} {chk['isHealthy']}")
            if chk['isHealthy'] != True:
                break
    if sys.argv[1] == '-r':
        usrlist = cf.readFromDB()
        print(' name  birthday  orgCode   pw   orgUrl')
        for i in usrlist:
            print(f"{i[0]} {i[1]} {i[2]} {i[3]} {i[4]}")
    if sys.argv[1] == '-u':
        API.updateQuestionChange()
except IndexError:
    pass